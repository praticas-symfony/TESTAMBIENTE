Edición estándar de Symfony
Bienvenido a Symfony Standard Edition, una Symfony completamente funcional
aplicación que puede usar como esqueleto para sus nuevas aplicaciones.
Para obtener detalles sobre cómo descargar y comenzar a usar Symfony, consulte el
Capítulo de instalación de la documentación de Symfony.

¿Qué hay adentro?
Symfony Standard Edition está configurado con los siguientes valores predeterminados:


Un AppBundle que puede usar para comenzar a codificar;


Twig como el único motor de plantillas configurado;


Doctrina ORM / DBAL;


Swiftmailer;


Anotaciones habilitadas para todo.


Viene preconfigurado con los siguientes paquetes:


FrameworkBundle: el paquete básico de framework de Symfony


SensioFrameworkExtraBundle: agrega varias mejoras, incluidas
capacidad de anotación de plantilla y enrutamiento


DoctrineBundle: agrega soporte para Doctrine ORM


TwigBundle: agrega soporte para el motor de plantillas Twig


SecurityBundle: agrega seguridad al integrar la seguridad de Symfony
componente


SwiftmailerBundle - Agrega soporte para Swiftmailer, una biblioteca para
mandando correos electrónicos


MonologBundle: agrega soporte para Monolog, una biblioteca de registro


AsseticBundle: agrega soporte para Assetic, un procesamiento de activos
biblioteca


WebProfilerBundle (en dev / test env): agrega funcionalidad de creación de perfiles y
la barra de herramientas de depuración web


SensioDistributionBundle (en dev / test env): agrega funcionalidad para
configurar y trabajar con distribuciones de Symfony


SensioGeneratorBundle (en dev / test env) - Agrega generación de código
capacidades


Todas las bibliotecas y paquetes incluidos en Symfony Standard Edition son
publicado bajo la licencia MIT o BSD.
¡Disfrutar!

[1]:  https://symfony.com/doc/2.3/book/installation.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/2.3/book/doctrine.html
[8]:  https://symfony.com/doc/2.3/book/templating.html
[9]:  https://symfony.com/doc/2.3/book/security.html
[10]: https://symfony.com/doc/2.3/cookbook/email.html
[11]: https://symfony.com/doc/2.3/cookbook/logging/monolog.html
[12]: https://symfony.com/doc/2.3/cookbook/assetic/asset_management.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
